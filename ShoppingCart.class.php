
<?php
class ShoppingCart
{
    public $cart;
    public $product;

    public function __construct(){
        global $products;

        if (!isset($_SESSION['cart']))  $_SESSION['cart'] = [];
        $this->cart = &$_SESSION['cart'];
        $this->products = $products;
    }

    public function AddProduct(){
        $result           = [];
        $result['status'] = 0;
        $result['msg']    = [];

        if ($_SERVER['REQUEST_METHOD'] != 'POST'){
            $result['msg'] = "Invalid submit method spotted";
        }else{
            $product = $_POST['product'] ?? '';
            if ($product != ''){
                $product_info = $this->getProduct($product);
    
                if ($product_info){
                    $result['status'] = 1;
                    $result['msg'] = "Product [{$product}] add to cart";
                    if (!isset($this->cart[$product])){
                        $product_info['quantity'] = 1;
                        $this->cart[$product] = $product_info;
                    }else{
                        $this->cart[$product]['quantity']++;
                    }
                }else{
                    $result['msg'] = "Invalid product [{$product}] found";
                }
                
            }else{
                $result['msg'] = "Product is missing";
            }
        }
        return $result;
    }

    public function DeleteProduct(){
        $result           = [];
        $result['status'] = 0;
        $result['msg']    = [];

        if ($_SERVER['REQUEST_METHOD'] != 'POST'){
            $result['msg'] = "Invalid submit method spotted";
        }else{
            $product = $_POST['product'] ?? '';
            if ($product != ''){
                if (isset($this->cart[$product])){
                   unset($this->cart[$product]);
                   $result['status'] = 1;
                   $result['msg'] = "product [{$product}] has been deleted from cart";
                }else{
                    $result['msg'] = "Invalid product [{$product}] found";
                }
                
            }else{
                $result['msg'] = "Product is missing";
            }
        }
        return $result;
    }

    public function Get(){
        return $this->cart;
    }

    public function Empty(){
        session_destroy();
        return true;
    }

    public function getSubTotal(){
        $products = $this->cart ?? [];
        $subTotal = 0;
        if (count($products)){
            
            foreach ($products as $product){
                $subTotal += $product['quantity'] * $product['price'];
            }
        }

        return $subTotal;
    }
   
    private function getProduct($product_name){
        
        $index = array_search($product_name,array_column($this->products,'name'));
        if (is_numeric($index)){
            return $this->products[$index];
        }else{
           
            return false;
        }
    }
}
?>