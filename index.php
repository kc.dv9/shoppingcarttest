<?php

session_start();

$allowed_class = ['ShoppingCart'];
foreach ($allowed_class as $class){
    include($class.'.class.php');
}


$products = [
                [ "name" => "Sledgehammer", "price" => 125.75],
                [ "name" => "Axe", "price" => 190.50 ],
                [ "name" => "Bandsaw", "price" => 562.131 ],
                [ "name" => "Chisel", "price" => 12.9 ],
                [ "name" => "Hacksaw", "price" => 18.45 ],
            ];


if (isset($_GET['class']) && isset($_GET['method']) && in_array($_GET['class'], $allowed_class)){

    $result = '';
    
    if (class_exists($_GET['class'])){
        $methods = get_class_methods($_GET['class']);
        $Obj = new $_GET['class'];
        if (in_array($_GET['method'], $methods)){
            eval("\$result = \$Obj->{$_GET['method']}();");

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
            {
                header('Content-type: application/json');
                echo json_encode($result);
                exit;
            }

        }else{
            die("Invalid method found");
        }

    }else{
        die("Invalid class found");
    }
}

?>
<html lang="en">
  <base href="/" />
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="KC KHU">
    <meta http-equiv="Pragma" content="no-cache">
    <title>Shopping Cart Practical Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
<body>

 
<div class="container-fluid" style="margin:10px;">
  <h1>List of Products</h1>
  <div class="row">
    <div class="col-sm-4" style="border:1px solid #C0C0C0;padding:5px;">
    
        
        <?php foreach ($products as $product){ ?>
            <div class="col-sm-3" style="padding:5px;">
              <?=$product['name']?> <br />
              NZD <?= number_format($product['price'],2,'.','')?><br />
              <a href="#" class="btn-sm btn-primary btn-add-cart" data-id="<?=htmlspecialchars($product['name'])?>">Add to Cart</a>
             </div>
        <?php } ?>
        <div class="alert alert-success col-sm-12" role="alert" id="sys-msg" style="display:none;"></div>
        <div class="alert alert-warning col-sm-12" role="alert" id="sys-msg-error" style="display:none;"></div>
    </div>
    <div class="col-sm-6" style="margin-left:10px;float:left;border:1px solid #C0C0C0;padding:5px;">
         
        <?php 
          $ShoppingCart = new ShoppingCart();
          foreach ((array)$ShoppingCart->Get() as $product){ 
        ?>    
        <div class="row" style="padding:5px;">
            <div class="col-sm-3"><?=htmlspecialchars($product['name'])?></div>
            <div class="col-sm-2">NZD <?=number_format($product['price'],2,'.','')?></div>
            <div class="col-sm-2"><?=htmlspecialchars($product['quantity'])?></div>
            <div class="col-sm-3">NZD <?=number_format($product['price'] * $product['quantity'],2,'.','')?></div>
            <div class="col-sm-2"><a href="#" class="btn-sm btn-warning btn-delete-cart" data-id="<?=htmlspecialchars($product['name'])?>">Delete</a> </div>
        </div>
        <?php } ?>
        <div class="row" style="padding:5px;" >
            <div class="col-sm-7" style="text-align:right;">SubTotal</div>
            <div class="col-sm-3" style="">NZD <?=number_format($ShoppingCart->getSubTotal(),2,'.','')?></div>
            <div class="col-sm-2"><a href="#" class="btn-sm btn-danger btn-empty-cart" data-id="<?=htmlspecialchars($product['name'])?>">Empty Cart</a> </div>
        </div>
      
    </div>
    
</div>

</div>
<script>
  $(document).ready(function(){
       $('.btn-add-cart').on('click', function(event){
           event.preventDefault();
           var selected_item = $(this).attr('data-id');
           console.log(selected_item);
           $('#sys-msg').html('Loading...');
           $('#sys-msg').show();

           $.post('/cart/AddProduct', {product: selected_item}, function(result){
                
                if (result.status == 1){
                    $('#sys-msg').html(result.msg).slideDown().delay('300').slideUp();
                    location.reload();
                }else{
                    $('#sys-msg').html('').hide();
                    $('#sys-msg-error').html(result.msg).slideDown().delay('1000').slideUp();
                }
                
           })
           
       });

       $('.btn-delete-cart').on('click', function(event){
            
            $('#sys-msg').html('Loading...');
            $('#sys-msg').show();
            

            var selected_item = $(this).attr('data-id');
            $.post('/cart/DeleteProduct', {product: selected_item}, function(result){
                
                if (result.status == 1){
                    $('#sys-msg').html(result.msg).slideDown().delay('300').slideUp();
                    location.reload();
                    
                }else{
                    $('#sys-msg').html('').hide();
                    $('#sys-msg-error').html(result.msg).slideDown().delay('1000').slideUp();
                }
                
           })
       });

       $('.btn-empty-cart').on('click', function(event){
            
            $('#sys-msg').html('Loading...');
            $('#sys-msg').show();
            var selected_item = $(this).attr('data-id');
            $.post('/cart/Empty', {}, function(result){
                    location.reload();
           })
       });
  })
</script>
</body>
</html>

